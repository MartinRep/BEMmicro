package ie.gmit.bem.domain.enumeration;

/**
 * The Category enumeration.
 */
public enum Category {
    FACE, BODY, MEN, DENTAL, HAIR, HEALTH, WELLBEING
}
